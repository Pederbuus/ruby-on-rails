#Creating a new article will require:
class Article < ApplicationRecord
    # Deleting Associated Objects
    include Visible
    has_many :comments

    validates :title, presence: true
    validates :body, presence: true, length: { minimum: 10 }
  end
  