class CommentsController < ApplicationController
    def create
        #Need to find the Article in question, and from there create the comment.
      @article = Article.find(params[:article_id])
      @comment = @article.comments.create(comment_params)
      redirect_to article_path(@article)
    end
  
    private
      def comment_params
        params.require(:comment).permit(:commenter, :body)
      end
  end
  