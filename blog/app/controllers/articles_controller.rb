class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  # Request to simply create a new article, no checks.
  def new
    @article = Article.new
  end

  # When we wish to create and save the new article, we check.
  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end

  # Same as create new, only on a already excisting entry.
  # Deafult links to new.html.erb
  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path, status: :see_other
  end

  #Insure that all the parameters are checked before acceptance, links to models/article.rb
  private
    def article_params
      params.require(:article).permit(:title, :body)
    end
end
