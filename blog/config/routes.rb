Rails.application.routes.draw do
  #root of the webpage
  root "articles#index"

  #URL paths directing to a VIEW HTML page
    #get "/articles", to: "articles#index"
    #get "/articles/:id", to: "articles#show"
  
  #^^ Being done automaticly
  resources :articles do
    resources :comments
  end
end
